
%define version 96
%define platform x86_64-centos7-gcc8-opt
%define platformFixed x86_64_centos7_gcc8_opt

%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LCG_96LHCb_x86_64-centos7-gcc8-opt
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: LCG  %{version} %{platform} for LHCb
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/LCGLHCb-%{version}-%{platform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: LCG_%{version}LHCb_%{platformFixed}

Requires: LCG_96_AIDA_3.2.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_Boost_1.70.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_catboost_0.9.1.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_chardet_3.0.4_x86_64_centos7_gcc8_opt
Requires: LCG_96_CLHEP_2.4.1.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_COOL_3_2_1_x86_64_centos7_gcc8_opt
Requires: LCG_96_CORAL_3_2_1_x86_64_centos7_gcc8_opt
Requires: LCG_96_cppgsl_2.0.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_CppUnit_1.14.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_DD4hep_01_10_x86_64_centos7_gcc8_opt
Requires: LCG_96_doxygen_1.8.15_x86_64_centos7_gcc8_opt
Requires: LCG_96_eigen_3.3.7_x86_64_centos7_gcc8_opt
Requires: LCG_96_fastjet_3.3.2_x86_64_centos7_gcc8_opt
Requires: LCG_96_fftw_3.3.8_x86_64_centos7_gcc8_opt
Requires: LCG_96_flatbuffers_1.11.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_Frontier_Client_2.8.19_x86_64_centos7_gcc8_opt
Requires: LCG_96_gdb_8.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_graphviz_2.40.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_GSL_2.5_x86_64_centos7_gcc8_opt
Requires: LCG_96_HepMC_2.06.09_x86_64_centos7_gcc8_opt
Requires: LCG_96_HepPDT_2.06.01_x86_64_centos7_gcc8_opt
Requires: LCG_96_idna_2.8_x86_64_centos7_gcc8_opt
Requires: LCG_96_ipython_genutils_0.2.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_lapack_3.8.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_libgit2_0.28.2_x86_64_centos7_gcc8_opt
Requires: LCG_96_libunwind_1.3.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_libxml2_2.9.9_x86_64_centos7_gcc8_opt
Requires: LCG_96_lxml_4.3.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_matplotlib_2.2.4_x86_64_centos7_gcc8_opt
Requires: LCG_96_mpmath_1.1.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_networkx_2.2_x86_64_centos7_gcc8_opt
Requires: LCG_96_neurobayes_expert_3.7.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_oracle_18.3.0.0.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_pathos_0.2.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_pyanalysis_2.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_pygraphics_2.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_pyqt5_5.12_x86_64_centos7_gcc8_opt
Requires: LCG_96_Python_2.7.16_x86_64_centos7_gcc8_opt
Requires: LCG_96_pytools_2.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_QMtest_2.4.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_Qt5_5.12.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_rangev3_0.5.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_RELAX_root6_x86_64_centos7_gcc8_opt
Requires: LCG_96_ROOT_6.18.00_x86_64_centos7_gcc8_opt
Requires: LCG_96_sqlite_3280000_x86_64_centos7_gcc8_opt
Requires: LCG_96_tbb_2019_U7_x86_64_centos7_gcc8_opt
Requires: LCG_96_tensorflow_estimator_1.14.0_x86_64_centos7_gcc8_opt
Requires: LCG_96_Vc_1.4.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_vdt_0.4.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_veccore_0.5.2_x86_64_centos7_gcc8_opt
Requires: LCG_96_vectorclass_1.30p1_x86_64_centos7_gcc8_opt
Requires: LCG_96_wcwidth_0.1.7_x86_64_centos7_gcc8_opt
Requires: LCG_96_XercesC_3.1.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_xgboost_0.90_x86_64_centos7_gcc8_opt
Requires: LCG_96_xqilla_2.3.3_x86_64_centos7_gcc8_opt
Requires: LCG_96_xrootd_4.9.1_x86_64_centos7_gcc8_opt
Requires: LCG_96_packaging_19.0_x86_64_centos7_gcc8_opt

%description
LCG externals %{version} %{platform} for LHCb


%prep

%build

%install

%files

%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version

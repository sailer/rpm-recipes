#--- Version ------------------------------------------------------
set(heptools_version 100_LHCBCORE)

#---Core layer-----------------------------------------------------
include(heptools-100)

#---DD4hep Package---------------------------------------------
# Disable link with Geant4 and recompile...
set(DD4hep_01.16.01_geant4 "OFF")
LCG_external_package(DD4hep 01.16.01)

#---GitCondDB Package----------------------------------------------
LCG_external_package(GitCondDB       0.2.0)
LCG_user_recipe(
  GitCondDB
  GIT_REPOSITORY https://gitlab.cern.ch/lhcb/GitCondDB.git GIT_TAG <VERSION>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_CXX_STANDARD=<CMAKE_CXX_STANDARD>
  BUILD_COMMAND ${MAKE}
  DEPENDS libgit2 fmt gtest
)

LCG_top_packages( GitCondDB DD4hep )


Creating LHCb packages with the LCG Layered stack system
========================================================

The documentation for the layered stack can be found at:

https://gitlab.cern.ch/sft/lcgcmake/blob/master/documentation.markdown/how-to-create-layered-stacks.md


The file heptools-101_LHCBCORE.cmake defined the custom packages needed by LHCb.
To build them, start by modifying the file *setenv.sh* to customize the CMTCONFIG needed, then run

BEWARE: This build cannot be done within the LHCb environment, a clean environment is needed.

For example to build the gcc-db version of DD4hep and GitCondDB for LHCb, with 101_LHCBCORE as
the version of the tools, and 101 as the name to use for the RPM packages, run:

```
source setenv.sh
source setbuildopts.sh x86_64-centos7-gcc10-opt 101_LHCBCORE 101
./build.sh
./package.sh
./release.sh

```


#--- Version ------------------------------------------------------
set(heptools_version 97apython3_LHCB_2)

#---Core layer-----------------------------------------------------
include(heptools-97apython3)

#---Specific of this layer-----------------------------------------
# Additions
LCG_external_package(pythia8           240.lhcb4            ${MCGENPATH}/pythia8 )
LCG_external_package(powheg-box-v2     r3043.lhcb.rdynamic  ${MCGENPATH}/powheg-box-v2 )
LCG_external_package(lhapdf            6.2.3                ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
LCG_external_package(thepeg            1.9.2p1              ${MCGENPATH}/thepeg author=1.9.2p1 lhapdf=6.2.3)
LCG_external_package(herwig++          2.7.1                ${MCGENPATH}/herwig++  thepeg=1.9.2p1 )
LCG_external_package(photos++          3.56.lhcb1           ${MCGENPATH}/photos++ )
LCG_external_package(pythia6           427.2.lhcb           ${MCGENPATH}/pythia6 author=6.4.27 hepevt=200000 )
LCG_external_package(tauola++          1.1.6b.lhcb          ${MCGENPATH}/tauola++ author=1.1.6b )
LCG_external_package(crmc              1.5.6                ${MCGENPATH}/crmc )
LCG_external_package(starlight         r300                 ${MCGENPATH}/starlight )
LCG_external_package(rivet             2.7.2b               ${MCGENPATH}/rivet yoda=1.7.7 )
LCG_external_package(madgraph          2.7.2.atlas3         ${MCGENPATH}/madgraph )
LCG_remove_package(herwig3)
LCG_remove_package(thep8i)

#---DD4hep Package---------------------------------------------
# Disable link with Geant4 and recompile...
set(DD4hep_v01-12-01-53-g84fb4870_geant4 "OFF")
LCG_external_package(DD4hep v01-12-01-53-g84fb4870)


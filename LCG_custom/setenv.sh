
if [ ! -d lcgcmake ]; then 
  #git clone -b LCG_100 https://gitlab.cern.ch/lhcb-core/lcgcmake.git
  git clone -b LCG_101-patches https://gitlab.cern.ch/sft/lcgcmake.git 
fi

if [ ! -d lcgjenkins ]; then   
   git clone https://gitlab.cern.ch/sft/lcgjenkins.git 
fi 

if [ ! -d build ]; then
  mkdir build
fi

if [ ! -d build/.lcgcmake ]; then
  mkdir build/.lcgcmake
fi

export PATH=$PWD/lcgcmake/bin:$PATH
export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.19.3/Linux-x86_64/bin:${PATH}




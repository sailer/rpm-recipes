#!/bin/bash

if [ ! -d build ]; then
  mkdir build
fi

if [ ! -d build/.lcgcmake ]; then
  mkdir build/.lcgcmake
fi

DBGOPT=""
if [[ "$BINARY_TAG" == *"-dbg" ]]; then
  DBGOPT=--debug
fi


cd build
lcgcmake reset
echo lcgcmake config --version ${LCGCMAKEVER} --prefix ../install --compiler ${COMPILER} --toolchain=../heptools-${LCGCMAKEVER}.cmake $DBGOPT
lcgcmake config --version ${LCGCMAKEVER} --prefix ../install --compiler ${COMPILER} --toolchain=../heptools-${LCGCMAKEVER}.cmake $DBGOPT
lcgcmake install


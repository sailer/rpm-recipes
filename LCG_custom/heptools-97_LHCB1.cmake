#--- Version ------------------------------------------------------
set(heptools_version 97_LHCB1)

#---Core layer-----------------------------------------------------
include(heptools-97)

#---Specific of this layer-----------------------------------------
# Additions
#LCG_external_package(pythia8           240.lhcb4            ${MCGENPATH}/pythia8 )
#LCG_external_package(powheg-box-v2     r3043.lhcb.rdynamic  ${MCGENPATH}/powheg-box-v2 )
#LCG_external_package(lhapdf            6.1.6.cxxstd         ${MCGENPATH}/lhapdf author=6.1.6 usecxxstd=1 )
#LCG_external_package(thepeg            1.9.2p1              ${MCGENPATH}/thepeg author=1.9.2 lhapdf=6.1.6.cxxstd)
#LCG_external_package(herwig++          2.7.1                ${MCGENPATH}/herwig++  thepeg=1.9.2p1 )
#LCG_external_package(photos++          3.56                 ${MCGENPATH}/photos++ )
#LCG_external_package(pythia6           427.2                ${MCGENPATH}/pythia6 author=6.4.27 hepevt=200000 )
#LCG_external_package(tauola++          1.1.6b.lhcb          ${MCGENPATH}/tauola++ author=1.1.6b )
#LCG_external_package(crmc              1.5.6                ${MCGENPATH}/crmc )
#LCG_external_package(starlight         r300                 ${MCGENPATH}/starlight )
#LCG_remove_package(herwig3)
#LCG_remove_package(thep8i)

#LCG_remove_package(DD4hep)
#LCG_remove_package(Geant4)
#LCG_remove_package(Gaudi)
#LCG_remove_package(Garfield++)
#LCG_remove_package(acts_core)


#---DD4hep Package---------------------------------------------
# Disable link with Geant4 and recompile...
set(DD4hep_v01-12-01-39-ga87cf29_geant4 "OFF")
LCG_external_package(DD4hep v01-12-01-39-ga87cf29)

#---GitCondDB Package----------------------------------------------
LCG_external_package(GitCondDB       0.1.3)
LCG_user_recipe(
  GitCondDB
  GIT_REPOSITORY https://gitlab.cern.ch/lhcb/GitCondDB.git GIT_TAG <VERSION>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_CXX_STANDARD=<CMAKE_CXX_STANDARD>
  BUILD_COMMAND ${MAKE}
  DEPENDS libgit2 fmt gtest
)

#---Define the top level packages for this stack-------------------
# For both GitCOndDB and DD4hep
LCG_top_packages(GitCondDB DD4hep)

# For just the GitCondDB
#LCG_top_packages(GitCondDB)


#!/bin/bash

set -v
set -e
cd install/${LCGCMAKEVER}
../../lcgjenkins/extract_LCG_summary.py . ${BINARY_TAG} ${LCGVER} RELEASE

cd ../..
let rver=${LCGVER//[!0-9]/}+1
#./lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=${rver} --match="(.*yamlcpp*|.*DD4hep.*)" -o all.spec ./install/${LCGCMAKEVER}/LCG_externals_${BINARY_TAG}.txt 
#./lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=${rver} --match="(.*DD4hep.*)" -o all.spec ./install/${LCGCMAKEVER}/LCG_externals_${BINARY_TAG}.txt 
#./lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=${rver} --match="(.*GitCondDB.*)" -o all.spec ./install/${LCGCMAKEVER}/LCG_externals_${BINARY_TAG}.txt 
#./lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=${rver} --match="(.*GitCondDB.*|.*DD4hep.*|.*pydantic.*)" -o all.spec ./install/${LCGCMAKEVER}/LCG_externals_${BINARY_TAG}.txt 
./lcgjenkins/LCGRPM/package/createLCGRPMSpec.py --debug --rebuild-all -p ${BINARY_TAG} --release=${rver} --match="(.*DD4hep.*)" -o all.spec ./install/${LCGCMAKEVER}/LCG_externals_${BINARY_TAG}.txt 
rpmbuild -bb all.spec

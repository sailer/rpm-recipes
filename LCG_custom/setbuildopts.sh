
export BINARY_TAG=${1:-x86_64-centos7-gcc9-opt}
export LCGCMAKEVER=${2:-97_LHCB1}
export LCGVER=${3:-97}
export COMPILER=$(echo $BINARY_TAG | cut -d'-' -f3)
if [[ ${BINARY_TAG} == *dbg ]]; then
	export DEBUG="--debug"
else
	export DEBUG=""
fi
if [ -d build ]; then
	rm -rf build
fi

# To build:
# mkdir -p /tmp/lben/rpmbuild/rpmbuild/BUILD
# mkdir -p /tmp/lben/rpmbuild/rpmbuild/RPMS/noarch/
# QA_RPATHS=$[ 0x0001|0x0010|0x0002 ] rpmbuild -bb  ~/Compat_v1r19_FromCVMFS.spec
#
#

%define lhcb_maj_version 1
%define lhcb_min_version 19
%define lhcb_patch_version 0
%define lhcb_release_version 7
%define buildarea /tmp/bcouturi/rpmbuild
%define project Compat
%define projectUp COMPAT
%define lbversion v1r19

%global __os_install_post /usr/lib/rpm/check-buildroot

%define _topdir %{buildarea}/rpmbuild
%define tmpdir %{buildarea}/tmpbuild
%define _tmppath %{buildarea}/tmp

Name: %{projectUp}
Version: %{lhcb_maj_version}.%{lhcb_min_version}.%{lhcb_patch_version}
Release: %{lhcb_release_version}
Vendor: LHCb
Summary: %{project}
License: GPL
Group: LHCb
BuildRoot: %{tmpdir}/%{name}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: %{projectUp}=%{lhcb_maj_version}.%{lhcb_min_version}.%{lhcb_patch_version}
Provides: %{projectUp}_v%{lhcb_maj_version}=%{lhcb_maj_version}.%{lhcb_min_version}.%{lhcb_patch_version}

        
%description
%{project}

%install
mkdir -p ${RPM_BUILD_ROOT}/opt/LHCbSoft/lhcb/%{projectUp}
cd  ${RPM_BUILD_ROOT}/opt/LHCbSoft/lhcb/%{projectUp} && cp -ar /cvmfs/lhcb.cern.ch/lib/lhcb/%{projectUp}/%{projectUp}_%{lbversion} .


%post

%postun

%clean

%files
%defattr(-,root,root)
/opt/LHCbSoft/lhcb/%{projectUp}/%{projectUp}_%{lbversion}

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version

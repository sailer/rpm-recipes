

# contribPlatform Should be passed as an argument

%define contribName iwyu
%define contribPlatform %{platform}
%define contribPlatformFixed  %(echo %{contribPlatform} | sed -e 's/-/_/')
%define contribDir /opt/LHCbSoft/contrib

%define url  https://github.com/pseyfert/include-what-you-use/archive
%define archname %{contribVersion}.zip
%define iwyudirname include-what-you-use-%{contribVersion}
%define llvmversion 3.9
%define gccversion 4.9.3
%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot


Name: %{contribName}_%{contribPlatformFixed}
Version: %{contribVersion}
Release: %{release}
Vendor: Paul Seyfert & include-what-you-use team
Summary: Contrib %{contribName} %{contribPlatform}
License: BSD
Group: contrib
Source: %{url}
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: x86_64
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}

%description
Include what you use

%prep
## Getting the ZIP
echo "Using platform <%{contribPlatform}>"
mkdir -p %_topdir/SOURCES
cd %_topdir/SOURCES
wget %{url}/%{archname}

mkdir -p %_topdir/BUILD
cd  %_topdir/BUILD
unzip -o %_topdir/SOURCES/%{archname}
source /cvmfs/lhcb.cern.ch/lib/lcg/external/llvm/%{llvmversion}/%{contribPlatform}/setup.sh

%build
source /cvmfs/lhcb.cern.ch/lib/lcg/external/llvm/%{llvmversion}/%{contribPlatform}/setup.sh
mkdir -p  %_topdir/BUILD/build
cd  %_topdir/BUILD/build
/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.7.2/Linux-x86_64/bin/cmake  -DIWYU_LLVM_ROOT_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/external/llvm/%{llvmversion}/%{contribPlatform}/ -DCMAKE_INSTALL_PREFIX=${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform} ../%{iwyudirname}
make

%install
cd  %_topdir/BUILD/build
make install
for ext in sh csh; do
    sed -e "s,@contribPlatform@,%{contribPlatform},g" \
        -e "s,@gccversion@,%{gccversion},g" \
        -e "s,@contribVersion@,%{contribVersion},g" \
        %{recipedir}/setup.${ext}.in > ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}/setup.${ext}
done

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version

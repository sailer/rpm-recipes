#!/bin/bash

# Find current absolute path
filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
CURRENT=$(cd  $dir;pwd)

# Get host os as an argument
if [ $# -eq 0 ]; then
   echo "Please specify the version to build"
  exit 1
fi


VERSION=${1}
CP=${2:-x86_64-centos7}
RELEASE=${3:-1}

mkdir -p /tmp/rpmbuild/BUILD
mkdir -p /tmp/rpmbuild/SOURCES
mkdir -p /tmp/rpmbuild/RPMS
set -v
rpmbuild -bb --define "platform ${CP}" --define "recipedir ${CURRENT}" --define "contribVersion ${VERSION}" --define "release ${RELEASE}" iwyu.spec




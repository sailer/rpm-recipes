# LLVM/Clang build

The script [build_clang.sh](build_clang.sh) download LLVM and Clang sources to
build them with CMake and create the RPM (using cpack).

This is a preliminary version that builds clang 4.0.1 for x86_64-centos7.

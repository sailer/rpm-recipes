Tools to prepare CMake RPMS for LHCb
=====================================

doit.sh shows how to do it.
Current version prepares the latest version.


Copy produced file to /eos/project/l/lhcbwebsites/www/lhcb-rpm/incubator/

then:

```
createrepo --workers=1 --update /eos/project/l/lhcbwebsites/www/lhcb-rpm/incubator/
```



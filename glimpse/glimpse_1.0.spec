

%define contribName glimpse
%define contribVersion 1.0.0
%define contribPlatform Linux-x86_64
%define contribPlatformFixed Linux_x86_64
%define contribDir /opt/LHCbSoft/contrib
%define urlPrefix /cvmfs/lhcb.cern.ch/lib/lhcb/COMPAT/COMPAT_v1r19/InstallArea/x86_64-slc6-gcc62-opt/bin
%define _topdir /tmp/rpmbuild
%define tmpdir /tmp/tmpbuild
%define _tmppath /tmp/tmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot


Name: %{contribName}_%{contribVersion}
Version: 1.0.0
Release: 2
Vendor: Kitware
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: BSD
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: x86_64
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}


%description
%{contribName} %{contribVersion}


%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}/bin
cp -ar %{urlPrefix}/* ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}/bin

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}



%post

%postun

%clean

%define date    %(echo `LC_ALL="C" date +"%a %b %d %Y"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version

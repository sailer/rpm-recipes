#!/bin/sh -ex

SPEC=${1:-glimpse_4.18.6.spec}

export TMPDIR=${TMPDIR:-/tmp}
mkdir -p $TMPDIR/rpmbuild/{BUILD,SOURCES,RPMS}
sudo yum-builddep -y ${SPEC}
rpmbuild --define="tmproot $TMPDIR/rpmbuild" -bb ${SPEC}



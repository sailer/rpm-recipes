Custom LCG externals
====================

Description
-----------
Tools to rebuild LCG externals on top of an existing LCG release.

Pre-conditions
--------------

The scripts in this directory use LCGCMake to rebuild externals "the LCG way"

The environment should be clear of LHCb specific variables e.g. setting CMTCONFIG can confuse LCGCMake.


Tools
-----

The tools use LCGCMake, as well as other tools from EP-SFT to prepare the RPMs.
The RPMs are generated in /tmp/rpmbuild and there are scripts to release tose RPMs directly to EOS.

A LHCb fork of LCGCMake is normally taken from: https://gitlab.cern.ch/lhcb-core/lcgcmake






#!/bin/bash
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)




#
# Script to produce custom DD4hep RPMs for a version of LCG
# It can be invoked like so:
# build_dd4hep_rpm.sh x86_64-centos7-gcc8-opt x86_64-centos7 8.2.0 95
# (c.f. defaults below).
#
# This requires /cvmfs/sft.cern.ch to run and does not check the consistency of the parameters

BINARY_TAG=${1:-x86_64-centos7-gcc9-opt}
export CMTCONFIG=${BINARY_TAG}
HOST_OS=${2:-x86_64-centos7}
GCC_VERSION=${3:-9.2.0}
HEPTOOLS_VERSION=${4:-96b}

latest_tag=$(${SCRIPT_LOC}/find_latest_dd4hep_tag.sh)
echo "Latest DD4hep is: ${latest_tag}"

echo "Preparing DD4hep ${latest_tag} for ${BINARY_TAG}"

source /cvmfs/sft.cern.ch/lcg/releases/gcc/${GCC_VERSION}/${HOST_OS}/setup.sh
ARCH=$(uname -m)
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.13.4/Linux-${ARCH}/bin:${PATH}

echo "Preparing the lcgcmake directory and patching the DD4hep version"
if [ ! -d lcgcmake ]; then
   # For LCG 95
   # git clone -b LCG_${HEPTOOLS_VERSION}b  https://gitlab.cern.ch/bcouturi/lcgcmake.git
   git clone -b LCG_${HEPTOOLS_VERSION}_LHCb https://gitlab.cern.ch/lhcb-core/lcgcmake.git
fi
sed -i "s/LCG_AA_project(DD4hep .*)/LCG_AA_project(DD4hep ${latest_tag})/"  lcgcmake/cmake/toolchain/heptools-dev-base.cmake

echo "Getting lcgjenkins needed for the RPM packaging"
if [ ! -d lcgjenkins ]; then
   git clone https://gitlab.cern.ch/sft/lcgjenkins.git
fi

echo "Preparing the build directory and invoking CMake"
if [ ! -d lcgcmake-build ]; then
  mkdir lcgcmake-build
  ( cd  lcgcmake-build 
	cmake -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases -DLCG_VERSION=${HEPTOOLS_VERSION} -DLCG_IGNORE='DD4hep' -DCMAKE_CXX_STANDARD=17 -DCMAKE_INSTALL_PREFIX=../lcgcmake-install ../lcgcmake )
fi

echo "Building the external packages"
cd lcgcmake-build
make -j 32 DD4hep
 
echo "Packaging the software"
cd ../lcgcmake-install/
../lcgjenkins/extract_LCG_summary.py . ${BINARY_TAG} ${HEPTOOLS_VERSION} RELEASE
 ../lcgjenkins/LCGRPM/package/createLCGRPMSpec.py -p ${BINARY_TAG} --release=2 --match="(.*DD4hep.*)" -o all.spec ./LCG_externals_${BINARY_TAG}.txt
rpmbuild -bb all.spec


